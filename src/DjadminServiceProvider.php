<?php

namespace djtalk\djadmin;

use Illuminate\Support\ServiceProvider;

class DjadminServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->loadRoutesFrom(__DIR__.'/routes.php');
        $this->loadMigrationsFrom(__DIR__.'/migrations');
        $this->loadViewsFrom(__DIR__.'/views', 'djadmin');
        $this->publishes([
            __DIR__.'/views' => base_path('resources/views/djtalk/djadmin'),
        ]);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->make('djtalk\djadmin\PostController');
    }
}
