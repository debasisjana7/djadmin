<?php

namespace djtalk\djadmin;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'name',
    ];
}
